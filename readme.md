# Introcept App Test
- Configuring database is not necessary since it uses csv file
```bash
	$ clone project
	$ composer install
	$ cpy .env.example .env
	$ php artisan serve
```
## instructions for docker
- This configures the docker
- only need to run docker-compose up -d for once.
```bash
	$ composer require --dev laravel/dusk
	$ php artisan dusk:install
	$ docker-compose up -d
```
- To build the app again use
```bash
	$ docker-compose up --build
```
- To further use
```bash
	$ docker-compose up
```
- To run php artisan commands in docker use
```bash
	$ docker-compose exec app php artisan . . .
```

## instructions for Testing
- This project uses dusk testing.
- Give proper APP_URL in .env file
- run testing in acpache.
```bash
	$ php artisan dusk
```

- For unit test
```bash
	$ vendor/bin/phpunit
```

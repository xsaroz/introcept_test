<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

class ProfileTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertSee('Introcept App');
    }

    /**
     * @test
     */
    public function check_if_home_page_gives_form_to_create_profile()
    {
        $response = $this->get('/');

        $response->assertStatus(200);

        $response->assertSee('Name');

        $response->assertSee('--select mode of contact--');

        $response->assertViewIs('profile.create');
    }

    /**
     * @test
     */
    public function test_if_form_passes_and_redirects_to_destination()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
             ->type('name', 'testName')
             ->type('email', 'test@test.com')
             ->radio('gender', 'M')
             ->type('phone', '123456789')
             ->type('address', 'my_address')
             ->type('nationality', 'nepali')
             ->type('date_of_birth', '01/01/1992')
             ->type('education', 'Bachelor')
             ->select('mode_of_contact', 1)
             ->press('Submit')
             ->assertPathIs('/profile');
        });
    }

    /**
     * @test
     */
    public function test_if_profile_index_page_has_table()
    {
        $response = $this->get('/profile');

        $response->assertSee('Profile List');
    }
}

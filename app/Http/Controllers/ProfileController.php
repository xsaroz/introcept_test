<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index()
    {
        $row = 0;
        if (($handle = fopen('my_file.csv', 'r')) !== false) {
            while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                $num = count($data);
                $saved_data = [];
                for ($c = 0; $c < $num; $c++) {
                    $saved_data[$c] = $data[$c];
                }
                $profiles[$row] = $saved_data;
                $row++;
            }
            fclose($handle);
        }

        return view('profile.index', compact('profiles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'              => 'required',
            'email'             => 'required',
            'gender'            => 'required',
            'phone'             => 'required|numeric',
            'address'           => 'required|max:150',
            'nationality'       => 'required|max:15',
            'date_of_birth'     => 'required',
            'education'         => 'required',
            'mode_of_contact'   => 'required',
        ]);

        $data = $this->getCsvArray($request);

        $this->generateCsv($data);

        return redirect()->route('profile.index');
    }

    public function generateCsv($data)
    {
        $handle = fopen('my_file.csv', 'a');
        fputcsv($handle, $data);
        fclose($handle);
    }

    public function getCsvArray($request)
    {
        return
            [
                $request->name,
                $request->email,
                $request->gender,
                $request->phone,
                $request->address,
                $request->nationality,
                $request->date_of_birth,
                $request->education,
                config('introcept-data.mode_of_contact')[$request->mode_of_contact],
            ];
    }
}
